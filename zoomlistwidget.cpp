#include "zoomlistwidget.h"

ZoomListWidget::ZoomListWidget(QWidget * parent):QListWidget(parent)
{

}

void ZoomListWidget::wheelEvent ( QWheelEvent * event )
{
    double scaleFactor = 1.15;
    if(event->delta() > 0) {
        // Zoom in
        scale = scale*scaleFactor;
        if(scale >200) scale = 200;
        setGridSize(QSize(scale*1.2,scale*1.2));
        setIconSize(QSize(scale,scale));
    }
    else {
        // Zooming out
        scale = scale/scaleFactor;
        if(scale < 50) scale = 50;
        setGridSize(QSize(scale*1.2,scale*1.2));
        setIconSize(QSize(scale,scale));
    }
}
