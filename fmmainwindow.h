#ifndef FMMAINWINDOW_H
#define FMMAINWINDOW_H

#include <QMainWindow>
//#include <QDirModel>
#include <QFileSystemModel>
#include <QMessageBox>
#include <QFile>
#include <QFileInfo>
#include <QMimeDatabase>
#include <QMimeType>
#include <QStandardItemModel>
#include <QFileIconProvider>
#include <QThreadPool>

#include "zoomlistwidget.h"

namespace Ui {
class FMMainWindow;
}

class FMMainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit FMMainWindow(QWidget *parent = 0);
    ~FMMainWindow();

private slots:
    void setRootIndex(const QModelIndex& modelindex);

private:
    Ui::FMMainWindow *ui;
    //QDirModel *model;
    QFileSystemModel *model;
    QStandardItemModel *modelinfo;
};

#endif // FMMAINWINDOW_H
