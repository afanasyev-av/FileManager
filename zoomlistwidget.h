#ifndef ZOOMLISTWIDGET_H
#define ZOOMLISTWIDGET_H

#include <QListWidget>
#include <QWheelEvent>

class ZoomListWidget : public QListWidget
{
Q_OBJECT
public:
    ZoomListWidget(QWidget * parent = 0);
private slots:
    void wheelEvent ( QWheelEvent * event );
public:
    int scale;
};

#endif // ZOOMLISTWIDGET_H
