#include "fmmainwindow.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    FMMainWindow w;
    w.show();

    return a.exec();
}
