#include "fmmainwindow.h"
#include "ui_fmmainwindow.h"

FMMainWindow::FMMainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::FMMainWindow)
{
    ui->setupUi(this);
    QObject::connect(ui->treeView, SIGNAL(clicked(const QModelIndex&)),
                     this, SLOT(setRootIndex(const QModelIndex&)));

    //model = new QDirModel(this);
    model = new QFileSystemModel(this);
    model->setRootPath("/");
    model->setFilter(QDir::Dirs | QDir::NoDotAndDotDot);
    ui->treeView->setModel(model);
    ui->treeView->setColumnHidden(1,true);
    ui->treeView->setColumnHidden(2,true);
    ui->treeView->setColumnHidden(3,true);
    ui->treeView->show();

    modelinfo = new QStandardItemModel(1,2,this);
    modelinfo->setHorizontalHeaderItem(0, new QStandardItem(QString("Name")));
    modelinfo->setHorizontalHeaderItem(1, new QStandardItem(QString("MIME-type")));

    ui->tableView->setModel(modelinfo);

    ui->listWidget->scale = 200;
    ui->listWidget->setViewMode(QListWidget::IconMode);
    ui->listWidget->setIconSize(QSize(ui->listWidget->scale,ui->listWidget->scale));
    ui->listWidget->setGridSize(QSize(ui->listWidget->scale*1.2,ui->listWidget->scale*1.2));
    ui->listWidget->setResizeMode(QListWidget::Adjust);

    ui->progressBar->setVisible(false);
}

FMMainWindow::~FMMainWindow()
{
    delete ui;
}

void FMMainWindow::setRootIndex(const QModelIndex& modelindex)
{
    QString pathName = model->filePath(modelindex);
    modelinfo->clear();
    ui->listWidget->clear();

    QDir *dir = new QDir(pathName);
    dir->setFilter(QDir::AllEntries | QDir::NoDotAndDotDot);
    QStringList qstrlist = dir->entryList();

    QMimeDatabase mimeDatabase;
    QMimeType mimeType;

    int i;
    QList<QStandardItem *> items;
    QFileIconProvider *iconFile = new QFileIconProvider();
    ui->progressBar->setMaximum(qstrlist.size());
    ui->progressBar->setValue(0);
    ui->progressBar->setVisible(true);
    for(i=0;i<qstrlist.size();i++){
        QString fileName;
        if (pathName.right(1) == "/"){
            fileName = pathName + qstrlist[i];
        }
        else{
            fileName = pathName + "/" + qstrlist[i];
        }
        QFileInfo file_info = QFileInfo(QFile(fileName));
        mimeType = mimeDatabase.mimeTypeForFile(file_info);
        items.clear();
        items.append(new QStandardItem(qstrlist[i]));
        items.append(new QStandardItem(mimeType.name()));
        modelinfo->appendRow(items);

        QString file_type = mimeType.name().left(5);
        if(file_type == "image"){
            QListWidgetItem *li = new QListWidgetItem(QPixmap(fileName).scaled(QSize(200,200),Qt::KeepAspectRatio),qstrlist[i]);
            ui->listWidget->addItem(li);
        }
        else{
            QPixmap pm;
            QIcon ic;
            #if defined(Q_OS_WIN)
            ic = iconFile->icon(file_info);
            #else
            ic = QIcon::fromTheme(mimeType.genericIconName());
            #endif
            pm = ic.pixmap(ic.actualSize(QSize(200,200)));
            QListWidgetItem *li = new QListWidgetItem(pm.scaled(QSize(200,200)),qstrlist[i]);
            ui->listWidget->addItem(li);
        }
        ui->progressBar->setValue( i );
    }
    ui->progressBar->setVisible(false);
}

